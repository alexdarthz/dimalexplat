using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemSpawn : MonoBehaviour
{
    public GameObject cherry;
    private GameObject block;
    [SerializeField] List<GameObject> children = new List<GameObject>();

    private void Start()
    {

        for (int i = 0; i < transform.childCount; i++)//Проходимся по объектам
        {
            block = transform.GetChild(i).gameObject;
            children.Add(block);
        }
        for (int i = 0; i < Random.Range(3,children.Count); i++)
        {
            int dead = Random.Range(0, children.Count);
            Instantiate(cherry, new Vector3(children[dead].transform.position.x, children[dead].transform.position.y + 1.0f, 150.0f), Quaternion.identity);
            children.RemoveAt(dead);
        }
    }
}