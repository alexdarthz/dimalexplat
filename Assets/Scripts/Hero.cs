using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
    [SerializeField] public int points;
    [SerializeField] private float speed = 3f;
    [SerializeField] private int lives = 5;
    [SerializeField] private float jumpforce = 0.005f;
    public static Hero Instance{get;set;}
    private bool isGrounded = false;
    private Rigidbody2D rb;
    private Animator anim;
    [SerializeField] private SpriteRenderer sprite;

    public GameObject conuter;

    private States State
    {
        get{return(States)anim.GetInteger("state");}
        set{anim.SetInteger("state",(int)value);}
    }

    private void Awake()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
        anim = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void Start() {
        points = 0;
    }

    private void Run()
    {
        if(isGrounded) State = States.run;
        Vector3 dir = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed * Time.deltaTime);
        sprite.flipX = dir.x < 0.0f;
    }

    private void Jump()
    {
        rb.AddForce(transform.up * jumpforce, ForceMode2D.Impulse);
    }
    private void CheckGround()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = collider.Length > 1;
        if(!isGrounded) State = States.jump;
    }
    public void GetDamage()
    {
        lives = -1;
        print("Жизней: " + lives);
    }
    public enum States
    {
        idle,
        run,
        jump
    }
    private void FixedUpdate() {
        CheckGround();
    }
    private void Update()
    {
        if(isGrounded) State = States.idle;
        if (Input.GetButton("Horizontal"))
            Run();
        if (isGrounded && Input.GetButton("Jump"))
            Jump();

    }
}
